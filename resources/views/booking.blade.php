@include('layouts.app')
@include('layouts.header')

<section id="content"> 
    <div class="container">
  <form action="booking_form_submit" method="post" class="booking-form col s6 require-validation" data-cc-on-file="false"
                        data-stripe-publishable-key="pk_test_nHdnFVtPkI4d8ELIgOlgQ6F3"
                        id="payment-form">
    
@csrf
<input name="customer_id" type="hidden"  id="booking_customer_id" value="">
   <ul class="stepper linear horizontal">
   <li class="step active">
      <div class="step-title waves-effect">Shipper Information
       <p>Please enter your infomation and proceed to next step so we can build your account</p>
      </div>
      
      <div class="step-content">
         <!-- Your step content goes here (like inputs or so) -->
         <h3 class="center">Shipper Information</h3>
         <div class="row">
         <div class="col s12 m4 l4">
           <label for="name">Full Name</label>
          <input name="name" type="text" maxlength="30" id="name" class="form-control1 required validate valid">
         
        </div>
        <div class="col s12 m4 l4">
          <label for="mobile_no">Mobile Number </label>
          <input name="mobile_no" type="text" 
                   onkeypress="return onlyNumberKey(event)" maxlength="20" id="mobile_no" class="form-control1 required validate valid">
          
        </div>
       
         <div class="col s12 m4 l4">
          <label for="comp_name">Company Name (if applicable)</label>
          <input name="comp_name" type="text" maxlength="30" id="comp_name" class="form-control1 validate valid">
          
        </div>
      </div>
        
      <div class="row">
       
        <div class="col s12 m4 l4">
          <label for="email">Email </label>
          <input name="email" type="text" maxlength="30" id="email" class="form-control1 required validate valid"  >
          
        </div>

        <div class="col s12 m4 l4">
        <label for="origin">Origin </label>
        <select name="origin" id="origin" class="form-control1 required validate valid" aria-invalid="false"  required>
           <!-- <option value="" selected disabled>Select Origin</option> -->
            <option value="UK" selected>United Kingdom</option>
        </select>
          
        </div>

        <div class="col s12 m4 l4">
          <label for="postcode">Postcode  </label>
         <!-- <input name="postcode" type="text" maxlength="10" onchange="" onkeypress="" id="postcode" class="form-control1 required validate valid" > -->
          
          <div id="postcode_lookup"></div>

<!-- <input id="line2" type="text" placeholder="Address Line 2" /> -->
 <input id="line3" type="hidden" placeholder="Address Line 3" />
 <input id="line4" type="hidden" placeholder="Address Line 4" />
<!-- <input id="town" type="text" placeholder="Town" /> -->
 <input name="postcode" id="postcode" type="hidden" placeholder="Postcode" required />
<script>
$('#postcode_lookup').getAddress({
    api_key: 'WsFh-kMOz-aNLu-biyD',
    output_fields: {
        // Form element ID's have been added below
        line1: '#street',
        line2 : '#address',
        line3: '#line3',
        line4: '#line4',
        town: '#city'
    }
});
var addr=document.getElementById("address").value;
document.getElementById("address").value=addr+ " " +document.getElementById("line3").value+" "+document.getElementById("line4").value;

</script>

        </div>

      </div>
     
      

      <div class="row">
        <div class="col s12 m6 l3">
          <label for="city">City </label>
          <input name="city" type="text"  id="city" class="form-control1 required validate valid" >
        </div>
        <div class="col s12 m6 l3">
          <label for="street">Street </label>
          <input id="street" type="text" class="form-control1 required validate valid" placeholder="Street" required>
        </div>
        <div class="col s12 m6 l6">
          <label for="address">Address (Further details if any)</label>
          <textarea name="address" id="address" class="materialize-textarea" length="120" height="42px"></textarea>  
        </div>
      </div>

         <div class="step-actions">
            <!-- Here goes your actions buttons -->
            <button class="waves-effect waves-dark btn next-step">NEXT</button>
         </div>
      </div>
   </li>
   <li class="step" >
      <div class="step-title waves-effect">Recipient Information</div>
      <div class="step-content" style="display: none;">
        <h3 class="center">Recipient Information</h3>
         <!-- Your step content goes here (like inputs or so) -->

      <div class="row">
        <div class="col s12 m6 l4">
          <label for="c_mobile_no">Mobile Number </label>
          <input name="c_mobile_no" type="text" onkeypress="return onlyNumberKey(event)" maxlength="20" id="c_mobile_no" class="form-control1 required validate valid" >
          
        </div>
        <div class="col s12 m6 l4">
          <label for="c_name">Name </label>
          <input name="c_name" type="text" maxlength="30" id="c_name" class="form-control1 required validate valid" >
          
        </div>
    

   
        <div class="col s12 m6 l4">
          <label for="c_comp_name">Company Name(if applicable) </label>
          <input name="c_comp_name" type="text" maxlength="30" id="5" class="form-control1  validate valid" >
          
        </div>
      </div>

      <div class="row">
         <div class="col s12 m6 l3">
       <label for="destination">Destination</label>
<select name="destination" id="destination" class="form-control1 required validate valid" onchange="GetSelectedTextValue(this)" required>
<?php if(isset($destination)) {?>  <option value="<?php echo $destination?>" selected><?php echo $destination?></option> 
          <?php } else {?>
                                            <option value="" selected disabled>Select Destination</option>

                                          <?php } ?> 
 

<option value="Afghanistan">Afghanistan</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antigua">Antigua</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Azores">Azores</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Barbuda">Barbuda</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bonaire">Bolivia</option>
<option value="Bonaire">Bonaire</option>
<option value="Bosnia Herzegovina">Bosnia Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Brazil">Brazil</option>
<option value="Brunei">Brunei</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Canary Islands">Canary Islands</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Cent. African Rep">Central African Rep</option>Cent. African Rep
<option value="Channel Islands">Channel Islands</option>Comoros
<option value="Chad">Chad</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Colombia">Colombia</option>
<option value="Comoros Islands">Comoros Islands</option>
<option value="Congo">Congo</option>
<option value="Congo Dem. Republic">Congo Dem. Republic</option>
<option value="Cook Islands">Cook Islands</option>

<option value="Costa Rica">Costa Rica</option>
<option value="Croatia">Croatia</option>
<option value="Curacao">Curacao</option>
<option value="Cuba">Cuba</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Rep">Dominican Rep</option>
<option value="East Timor">East Timor</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands">Falkland Islands</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France </option>
<option value="French Guyana">French Guyana</option>
<option value="Gabon">Gabon</option>
<option value="Gambia">Gambia</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam </option>
<option value="Guatemala">Guatemala</option>
<option value="Guinea">Guinea</option>
<option value="Guinea-Bissau">Guinea-Bissau</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran">Iran</option>
<option value="Iraq">Iraq</option>
<option value="Ireland (Republic of)">Ireland (Republic of)</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Ivory Coast">Ivory Coast</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea (South)">Korea (South)</option>
<option value="Korea (North)">Korea (North)</option>
<option value="Kosovo">Kosovo</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Laos">Laos</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libya">Libya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macau">Macau</option>
<option value="Macedonia">Macedonia</option>
<option value="Madagascar">Madagascar</option>
<option value="Malawi">Malawi</option>
<option value="Malaysia">Malaysia</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mexico">Mexico</option>
<option value="Moldova">Moldova</option>
<option value="Monaco">Monaco</option>
<option value="Montenegro">Montenegro</option>
<option value="Mongolia">Mongolia</option>
<option value="Monserrat">Monserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Namibia">Namibia</option>
<option value="Nepal">Nepal</option>
<option value="Nauru">Nauru</option>
<option value="Netherlands">Netherlands</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>Palestine State
<option value="Niue">Niue</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palestine State">Palestine State</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Reunion Island">Reunion Island</option>
<option value="Romania">Romania</option>
<option value="Russian Federation">Russian Federation</option>
<option value="Rwanda">Rwanda</option>
<option value="Samoa">Samoa</option>
<option value="San Marino">San Marino</option>
<option value="Saipan">Saipan</option>
<option value="Sao Tome & Principe">Sao Tome & Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore </option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="South Africa">South Africa</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="St Lucia">St Lucia</option>
<option value="St Maarten">St Maarten</option>
<option value="St Vincent">St Vincent</option>
<option value="St Barthelemy">St. Barthelemy</option>
<option value="St Eustatius">St. Eustatius</option>
<option value="St Kitts & Nevis">St. Kitts &amp; Nevis</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>

<option value="Syria">Syria</option>
<option value="Tahiti">Tahiti</option>
<option value="Taiwan">Taiwan</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania">Tanzania</option>
<option value="Thailand">Thailand</option>
<option value="Togo">Togo</option>
<option value="Trinidad & Tobago">Trinidad & Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks & Caicos Is.">Turks & Caicos Is.</option>
<option value="Tuvalu">Tuvalu</option>
<option value="U.A.E">U.A.E</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="Uruguay">Uruguay</option>
<option value="U.S. of America">U.S. of America</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnam">Vietnam</option>
<option value="Virgin Islands (Brit)">Virgin Islands</option>
<option value="Virgin Islands (US)">Virgin Islands (US)</option>
<option value="Yemen">Yemen</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
        </select>
          
        </div>
    
        <div class="col s12 m3 l3">
    <label for="d_postcode">Postcode  (If not known enter 0)</label>
<input name="d_postcode" type="text" maxlength="10" onchange="" onkeypress="" id="d_postcode" class="form-control1 required validate valid" >
          
        </div>


        
         <div class="col s12 m3 l3">
         <label for="d_city">City </label>
         <div class="city_pakistan">
         <select name="d_city" id="d_city_p" required style="display:none">
    <option value="" disabled selected>Select The City</option>
    <option value="Islamabad">Islamabad</option>
    <option value="" disabled>Punjab Cities</option>
    <option value="Ahmed Nager Chatha">Ahmed Nager Chatha</option>
    <option value="Ahmadpur East">Ahmadpur East</option>
    <option value="Ali Khan Abad">Ali Khan Abad</option>
    <option value="Alipur">Alipur</option>
    <option value="Arifwala">Arifwala</option>
    <option value="Attock">Attock</option>
    <option value="Bhera">Bhera</option>
    <option value="Bhalwal">Bhalwal</option>
    <option value="Bahawalnagar">Bahawalnagar</option>
    <option value="Bahawalpur">Bahawalpur</option>
    <option value="Bhakkar">Bhakkar</option>
    <option value="Burewala">Burewala</option>
    <option value="Chillianwala">Chillianwala</option>
    <option value="Chakwal">Chakwal</option>
    <option value="Chichawatni">Chichawatni</option>
    <option value="Chiniot">Chiniot</option>
    <option value="Chishtian">Chishtian</option>
    <option value="Daska">Daska</option>
    <option value="Darya Khan">Darya Khan</option>
    <option value="Dera Ghazi Khan">Dera Ghazi Khan</option>
    <option value="Dhaular">Dhaular</option>
    <option value="Dina">Dina</option>
    <option value="Dinga">Dinga</option>
    <option value="Dipalpur">Dipalpur</option>
    <option value="Faisalabad">Faisalabad</option>
    <option value="Ferozewala">Ferozewala</option>
    <option value="Fateh Jhang">Fateh Jang</option>
    <option value="Ghakhar Mandi">Ghakhar Mandi</option>
    <option value="Gojra">Gojra</option>
    <option value="Gujranwala">Gujranwala</option>
    <option value="Gujrat">Gujrat</option>
    <option value="Gujar Khan">Gujar Khan</option>
    <option value="Hafizabad">Hafizabad</option>
    <option value="Haroonabad">Haroonabad</option>
    <option value="Hasilpur">Hasilpur</option>
    <option value="Haveli Lakha">Haveli Lakha</option>
    <option value="Jatoi">Jatoi</option>
    <option value="Jalalpur">Jalalpur</option>
    <option value="Jattan">Jattan</option>
    <option value="Jampur">Jampur</option>
    <option value="Jaranwala">Jaranwala</option>
    <option value="Jhang">Jhang</option>
    <option value="Jhelum">Jhelum</option>
    <option value="Kalabagh">Kalabagh</option>
    <option value="Karor Lal Esan">Karor Lal Esan</option>
    <option value="Kasur">Kasur</option>
    <option value="Kamalia">Kamalia</option>
    <option value="Kamoke">Kamoke</option>
    <option value="Khanewal">Khanewal</option>
    <option value="Khanpur">Khanpur</option>
    <option value="Kharian">Kharian</option>
    <option value="Khushab">Khushab</option>
    <option value="Kot Addu">Kot Addu</option>
    <option value="Jauharabad">Jauharabad</option>
    <option value="Lahore">Lahore</option>
    <option value="Lalamusa">Lalamusa</option>
    <option value="Layyah">Layyah</option>
    <option value="Liaquat Pur">Liaquat Pur</option>
    <option value="Lodhran">Lodhran</option>
    <option value="Malakwal">Malakwal</option>
    <option value="Mamoori">Mamoori</option>
    <option value="Mailsi">Mailsi</option>
    <option value="Mandi Bahauddin">Mandi Bahauddin</option>
    <option value="Mian Channu">Mian Channu</option>
    <option value="Mianwali">Mianwali</option>
    <option value="Multan">Multan</option>
    <option value="Murree">Murree</option>
    <option value="Muridke">Muridke</option>
    <option value="Mianwali Bangla">Mianwali Bangla</option>
    <option value="Muzaffargarh">Muzaffargarh</option>
    <option value="Narowal">Narowal</option>
    <option value="Nankana Sahib">Nankana Sahib</option>
    <option value="Okara">Okara</option>
    <option value="Renala Khurd">Renala Khurd</option>
    <option value="Pakpattan">Pakpattan</option>
    <option value="Pattoki">Pattoki</option>
    <option value="Pir Mahal">Pir Mahal</option>
    <option value="Qaimpur">Qaimpur</option>
    <option value="Qila Didar Singh">Qila Didar Singh</option>
    <option value="Rabwah">Rabwah</option>
    <option value="Raiwind">Raiwind</option>
    <option value="Rajanpur">Rajanpur</option>
    <option value="Rahim Yar Khan">Rahim Yar Khan</option>
    <option value="Rawalpindi">Rawalpindi</option>
    <option value="Sadiqabad">Sadiqabad</option>
    <option value="Safdarabad">Safdarabad</option>
    <option value="Sahiwal">Sahiwal</option>
    <option value="Sangla Hill">Sangla Hill</option>
    <option value="Sarai Alamgir">Sarai Alamgir</option>
    <option value="Sargodha">Sargodha</option>
    <option value="Shakargarh">Shakargarh</option>
    <option value="Sheikhupura">Sheikhupura</option>
    <option value="Sialkot">Sialkot</option>
    <option value="Sohawa">Sohawa</option>
    <option value="Soianwala">Soianwala</option>
    <option value="Siranwali">Siranwali</option>
    <option value="Talagang">Talagang</option>
    <option value="Taxila">Taxila</option>
    <option value="Toba Tek Singh">Toba Tek Singh</option>
    <option value="Vehari">Vehari</option>
    <option value="Wah Cantonment">Wah Cantonment</option>
    <option value="Wazirabad">Wazirabad</option>
    <option value="" disabled>Sindh Cities</option>
    <option value="Badin">Badin</option>
    <option value="Bhirkan">Bhirkan</option>
    <option value="Rajo Khanani">Rajo Khanani</option>
    <option value="Chak">Chak</option>
    <option value="Dadu">Dadu</option>
    <option value="Digri">Digri</option>
    <option value="Diplo">Diplo</option>
    <option value="Dokri">Dokri</option>
    <option value="Ghotki">Ghotki</option>
    <option value="Haala">Haala</option>
    <option value="Hyderabad">Hyderabad</option>
    <option value="Islamkot">Islamkot</option>
    <option value="Jacobabad">Jacobabad</option>
    <option value="Jamshoro">Jamshoro</option>
    <option value="Jungshahi">Jungshahi</option>
    <option value="Kandhkot">Kandhkot</option>
    <option value="Kandiaro">Kandiaro</option>
    <option value="Karachi">Karachi</option>
    <option value="Kashmore">Kashmore</option>
    <option value="Keti Bandar">Keti Bandar</option>
    <option value="Khairpur">Khairpur</option>
    <option value="Kotri">Kotri</option>
    <option value="Larkana">Larkana</option>
    <option value="Matiari">Matiari</option>
    <option value="Mehar">Mehar</option>
    <option value="Mirpur Khas">Mirpur Khas</option>
    <option value="Mithani">Mithani</option>
    <option value="Mithi">Mithi</option>
    <option value="Mehrabpur">Mehrabpur</option>
    <option value="Moro">Moro</option>
    <option value="Nagarparkar">Nagarparkar</option>
    <option value="Naudero">Naudero</option>
    <option value="Naushahro Feroze">Naushahro Feroze</option>
    <option value="Naushara">Naushara</option>
    <option value="Nawabshah">Nawabshah</option>
    <option value="Nazimabad">Nazimabad</option>
    <option value="Qambar">Qambar</option>
    <option value="Qasimabad">Qasimabad</option>
    <option value="Ranipur">Ranipur</option>
    <option value="Ratodero">Ratodero</option>
    <option value="Rohri">Rohri</option>
    <option value="Sakrand">Sakrand</option>
    <option value="Sanghar">Sanghar</option>
    <option value="Shahbandar">Shahbandar</option>
    <option value="Shahdadkot">Shahdadkot</option>
    <option value="Shahdadpur">Shahdadpur</option>
    <option value="Shahpur Chakar">Shahpur Chakar</option>
    <option value="Shikarpaur">Shikarpaur</option>
    <option value="Sukkur">Sukkur</option>
    <option value="Tangwani">Tangwani</option>
    <option value="Tando Adam Khan">Tando Adam Khan</option>
    <option value="Tando Allahyar">Tando Allahyar</option>
    <option value="Tando Muhammad Khan">Tando Muhammad Khan</option>
    <option value="Thatta">Thatta</option>
    <option value="Umerkot">Umerkot</option>
    <option value="Warah">Warah</option>
    <option value="" disabled>Khyber Cities</option>
    <option value="Abbottabad">Abbottabad</option>
    <option value="Adezai">Adezai</option>
    <option value="Alpuri">Alpuri</option>
    <option value="Akora Khattak">Akora Khattak</option>
    <option value="Ayubia">Ayubia</option>
    <option value="Banda Daud Shah">Banda Daud Shah</option>
    <option value="Bannu">Bannu</option>
    <option value="Batkhela">Batkhela</option>
    <option value="Battagram">Battagram</option>
    <option value="Birote">Birote</option>
    <option value="Chakdara">Chakdara</option>
    <option value="Charsadda">Charsadda</option>
    <option value="Chitral">Chitral</option>
    <option value="Daggar">Daggar</option>
    <option value="Dargai">Dargai</option>
    <option value="Darya Khan">Darya Khan</option>
    <option value="Dera Ismail Khan">Dera Ismail Khan</option>
    <option value="Doaba">Doaba</option>
    <option value="Dir">Dir</option>
    <option value="Drosh">Drosh</option>
    <option value="Hangu">Hangu</option>
    <option value="Haripur">Haripur</option>
    <option value="Karak">Karak</option>
    <option value="Kohat">Kohat</option>
    <option value="Kulachi">Kulachi</option>
    <option value="Lakki Marwat">Lakki Marwat</option>
    <option value="Latamber">Latamber</option>
    <option value="Madyan">Madyan</option>
    <option value="Mansehra">Mansehra</option>
    <option value="Mardan">Mardan</option>
    <option value="Mastuj">Mastuj</option>
    <option value="Mingora">Mingora</option>
    <option value="Nowshera">Nowshera</option>
    <option value="Paharpur">Paharpur</option>
    <option value="Pabbi">Pabbi</option>
    <option value="Peshawar">Peshawar</option>
    <option value="Saidu Sharif">Saidu Sharif</option>
    <option value="Shorkot">Shorkot</option>
    <option value="Shewa Adda">Shewa Adda</option>
    <option value="Swabi">Swabi</option>
    <option value="Swat">Swat</option>
    <option value="Tangi">Tangi</option>
    <option value="Tank">Tank</option>
    <option value="Thall">Thall</option>
    <option value="Timergara">Timergara</option>
    <option value="Tordher">Tordher</option>
    <option value="" disabled>Balochistan Cities</option>
    <option value="Awaran">Awaran</option>
    <option value="Barkhan">Barkhan</option>
    <option value="Chagai">Chagai</option>
    <option value="Dera Bugti">Dera Bugti</option>
    <option value="Gwadar">Gwadar</option>
    <option value="Harnai">Harnai</option>
    <option value="Jafarabad">Jafarabad</option>
    <option value="Jhal Magsi">Jhal Magsi</option>
    <option value="Kacchi">Kacchi</option>
    <option value="Kalat">Kalat</option>
    <option value="Kech">Kech</option>
    <option value="Kharan">Kharan</option>
    <option value="Khuzdar">Khuzdar</option>
    <option value="Killa Abdullah">Killa Abdullah</option>
    <option value="Killa Saifullah">Killa Saifullah</option>
    <option value="Kohlu">Kohlu</option>
    <option value="Lasbela">Lasbela</option>
    <option value="Lehri">Lehri</option>
    <option value="Loralai">Loralai</option>
    <option value="Mastung">Mastung</option>
    <option value="Musakhel">Musakhel</option>
    <option value="Nasirabad">Nasirabad</option>
    <option value="Nushki">Nushki</option>
    <option value="Panjgur">Panjgur</option>
    <option value="Pishin Valley">Pishin Valley</option>
    <option value="Quetta">Quetta</option>
    <option value="Sherani">Sherani</option>
    <option value="Sibi">Sibi</option>
    <option value="Sohbatpur">Sohbatpur</option>
    <option value="Washuk">Washuk</option>
    <option value="Zhob">Zhob</option>
    <option value="Ziarat">Ziarat</option>
  </select>
</div>
          <input name="d_city" type="text" id="d_city" class="form-control1 required validate valid" >
                  </div>
        <div class="col s12 m3 l3">
          <label for="d_address">Address</label>
        <textarea id="d_address" name="d_address" class="materialize-textarea required validate valid" length="120" height="42px"></textarea>
            
        </div>
      </div>
      
     

      <div class="row">
        
      </div>





























      
         <div class="step-actions">
            <!-- Here goes your actions buttons -->
            <button class="waves-effect waves-dark btn next-step">NEXT</button>
         </div>
      </div>
   </li>
   <li class="step" >
      <div class="step-title waves-effect">Booking Details</div>
      <div class="step-content" style="display: none;">
        <h3 class="center">Recipient Information</h3>
         <!-- Your step content goes here (like inputs or so) -->
         <div class="row">
        <div class="col s12 m4 l2">
          <label for="num_pieces">No. of Pieces</label>
          <input name="num_pieces" type="text" value="1" maxlength="4" id="txtPcs" class="form-control1 numericfield required validate valid" onkeypress="" required>
          
        </div>
        <div class="col s12 m4 l2">
           <label for="txtShipValue">Shipment Value</label>
    <input name="ship_vaue" type="text" maxlength="10" id="ship_vaue" class="form-control1 required validate valid" placeholder="e.g 1 GBP">
         
        </div>
        <div class="col s12 m4 l2">
            <label for="txtShipValue">Shipping Type</label>
            <select name="country_code" id="country_code" class="form-control1 required validate valid">
                <option selected="selected" value="D">Doc</option>
                <option value="N">Non-Doc</option>
            </select>
        </div>
        <div class="col s12 m6 l6">
          <label for="ship_content">Shipment content description</label>
          <input name="ship_content" type="text" maxlength="100" id="ship_content" class="form-control1 validate valid">
          
        </div>
      </div>
        

      <div class="row">
      
        <div class="col s12 m6 l2">
          <label for="agent_code">Promo Code</label>
          <input name="agent_code" type="text" maxlength="100" onkeypress="" id="agent_code" class="form-control1 validate valid">
          
        </div>
      
          <div class="col s4 m2 l2">
            <label for="txtLength">Length (cm)</label>
            <input name="txtLength" type="text" oninput="setVolume()" maxlength="4" id="txtLength" class="form-control1 required validate valid btnweighttotal" placeholder="L">
          </div>
          <div class="col s4 m2 l2">
            <label for="txtLength">Width (cm)</label>
            <input name="txtWidth" type="text" oninput="setVolume()" maxlength="4" id="txtWidth" class="form-control1 required validate valid btnweighttotal" placeholder="W">  
          </div>
          <div class="col s4 m2 l2">
            <label for="txtLength">Height (cm)</label>
            <input name="txtHeight" oninput="setVolume()" type="text" maxlength="4" id="txtHeight" class="form-control1 required validate valid btnweighttotal" placeholder="H">
          </div>

          <div class="col s12 m6 l2">
        <label for="ship_vaue">Exp. Weight (Kg)</label>
          <select name="weight" id="weight" onchange="setTarrif()" class="form-control1 required validate valid" required>
            <?php if(isset($weight)) {?>  <option value="<?php echo $weight?>" selected><?php echo $weight?></option> 
          <?php } else {?>
                                            <option value="" selected disabled>Select Weight</option>

                                          <?php } ?> 
                                            <option value="0.5">0.5</option>
                                            <option value="1.0">1.0</option>
                                            <option value="1.5">1.5</option>
                                            <option value="2.0">2.0</option>
                                            <option value="2.5">2.5</option>
                                            <option value="3.0">3.0</option>
                                            <option value="3.5">3.5</option>
                                            <option value="4.0">4.0</option>
                                            <option value="4.5">4.5</option>
                                            <option value="5.0">5.0</option>
          </select>
        </div>
     
          
    
        <div class="col s12 m3 l2 ">
          <label for="volume">Volum.Wgt(cm) </label>
          <input name="volume" type="text" readonly="readonly" id="volume" class="form-control1 required validate valid" onkeypress="">  
        </div>
      </div>

      <div class="row">
        <div class="col s12 m6 l6">
        <label for="service">Service</label>
        <select class="form-control1 required validate valid" data-live-search="True" id="service_code" name="service_code" required>
              <option value="" selected disabled>Select Service</option>
              <option value="401">Collection Next Day (Monday-Friday)</option>
          </select>
        </div>
        <div class="col s12 m6 l6">
           <label for="instructions">Special Instruction</label>
           <input name="instructions" type="text" maxlength="30" id="instructions" class="form-control1 validate valid">
          
        </div>
        
      </div>

      <div class="row">
        
        <div class="col s12 m6 l4">
        <label for="pay_mode">Mode of Payment</label>
          <select name="pay_mode" id="pay_mode" class="form-control1 required validate valid" required>
              <!-- <option value="" selected disabled>Select Mode of Payment</option> -->
              <option value="Credit Card/Debit Card" selected>Credit Card / Debit Card</option>
          </select>
        </div>
        <div class="col s6 l4">
        <label for="tarrif">Tariff (£)</label>
         <input name="tarrif" type="text"  maxlength="5" id="tarrif" class="form-control1 required validate valid" readonly="readonly" >
         
        </div>
        
      </div>
         <div class="step-actions">
            <!-- Here goes your actions buttons -->
            <button onclick="setDetails()" class="waves-effect waves-dark btn next-step ">Next</button>
         </div>
      </div>
   </li>
   <li class="step">
      <div class="step-title waves-effect">Review</div>
      <div class="step-content" style="display: none;">
        <h3 class="center">Review</h3>
      <!-- Your step content goes here (like inputs or so) -->
          <div class="row">
            <div class="col s12 m6 l6">
              <div class="card">
                <div class="card-header">Shipper</div>
                <div class="card-content">
                  <div class="row">
                    <div class="col s12 m6 l6">
                         <label for="name">Full Name</label>
                        <input name="name_1" type="text" maxlength="30" id="name_1" class="form-control1" readonly="readonly" required>
                      </div>
                      <div class="col s12 m6 l6">
                        <label for="mobile_no_1">Mobile Number </label>
              <input name="mobile_no_1" type="text" maxlength="20" id="mobile_no_1" class=" form-control1" readonly="readonly" required>
                      </div>
                      
                  </div>
                  <div class="row">
                      <div class="col s12 m3 l3">
                        <label for="postcode_1">Postcode</label>
                        <input name="postcode_1" type="text" maxlength="10" onchange="" onkeypress="" id="postcode_1" class="form-control1" readonly="readonly" required>
                      </div>
                      <div class="col s12 m3 l3">
                        <label for="city_1">City </label>
                        <input name="city_1" type="text"  id="city_1" class="form-control1" readonly="readonly" required>
                      </div>
                      <div class="col s12 m6 l6">
                        <label for="address_1">Address</label>
                        <p id="address_1"></p>
                      </div>
                  </div>
                </div> 
              </div><!-- end card -->
            </div>
            <div class="col s12 m6 l6">
              <div class="card">
                <div class="card-header">Recipient</div>
                <div class="card-content">
                  <div class="row">
                    <div class="col s12 m6 l6">
                         <label for="c_name_1">Full Name</label>
                        <input name="c_name_1" type="text" maxlength="30" id="c_name_1" class="form-control1" readonly="readonly" required>
                      </div>
                      <div class="col s12 m6 l6">
                        <label for="c_mobile_no_1">Mobile Number </label>
                        <input name="c_mobile_no_1" type="text" maxlength="20" id="c_mobile_no_1" class=" form-control1" readonly="readonly" required>
                      </div>
                      
                  </div>
                  <div class="row">
                    <div class="col s12 m3 l3">
                      <label for="d_postcode_1">Postcode</label>
                      <input name="d_postcode_1" type="text" maxlength="10" onchange="" onkeypress="" id="d_postcode_1" class="form-control1" readonly="readonly" required>
                    </div>
                    <div class="col s12 m3 l3">
                      <label for="d_city_1">City </label>
                      <input name="d_city_1" type="text"  id="d_city_1" class="form-control1" readonly="readonly" required>
                    </div>
                    <div class="col s12 m6 l6">
                        <label for="d_address_1">Address</label>
                        <p id="d_address_1"></p> 
                    </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
          
         
      
         <div class="step-actions">
            <!-- Here goes your actions buttons -->
             <button class="waves-effect waves-dark btn next-step">NEXT</button>
         </div>
        </div>
      

                      
   </li>
   <li class="step">
      <div class="step-title waves-effect">Payment</div>
      <div class="step-content" style="display: none;">
        <h3 class="center">Payment</h3>
      <!-- Your step content goes here (like inputs or so) -->
    
          <div class='row'>
            <div class='col offset-l1 l4'>
              <label class='control-label'>Card Number</label> 
            <input  class='form-control1 card-number required validate valid' name="cardnumber" id="card-number" autocomplete='cc-name'  size='20'type='text'>
            </div>
            <div class='col s3 l2'>
              <label class='control-label'>CVC</label> 
              <input class='form-control1 card-cvc required validate valid' placeholder='ex. 311' size='4' type='text'>
            </div>
         
            <div class='col s3 l2'>
              <label class='control-label'>Expiration Month</label> 
              <input class='form-control1 card-expiry-month required validate valid' id="card-expiry-month" placeholder='MM' size='2'type='text' name='exp-date' autocomplete='cc-exp-month' >
            </div>
            <div class='col s3 l2'>
              <label class='control-label'>Expiration Year</label> 
              <input class='form-control1 card-expiry-year required validate valid' id="card-expiry-year" laceholder='YYYY' size='4'type='text' name='exp-date' autocomplete='cc-exp-year'>
            </div>
        
          </div>
          <div class="row">
          <div class='col offset-l1 s3 l2'>
              <label class='control-label'>Amount</label> 
            <input class='form-control1 amount required validate valid'  size='5' type='text' name='amount' id="amount" readonly="readonly">
            </div>
          </div>
         <div class="step-actions">
            <!-- Here goes your actions buttons -->
             <input class="btn waves-effect waves-dark btn waves-input-wrapper waves-input-wrapper" type="submit" id="paynow" value='Pay Now'></button>
         </div>
        </div>
      

                      
   </li>
</ul>
     

</form>
      
   
</div>
</section>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   <script>

 
  
     
     
      var destination=document.getElementById('destination').value;
        var weight=document.getElementById('weight').value;
        
        if(weight=="")
        {
             Swal.fire(
                "Weight is required to caculate Tarrif"
            );
        }
        
    $.ajax(
    {
        type: "GET",
        url: 'get_zone'+'/'+destination+'/'+weight,
        dataType: "json",
        beforeSend: function(){
            document.getElementById('tarrif').style.backgroundImage='url(https://www.drivethrurpg.com/shared_images/ajax-loader.gif)';
        },
        complete:function(data){
            document.getElementById('tarrif').style.backgroundImage='none';
        },
        success: function(result) {
           
           document.getElementById('tarrif').value=result;
          
          
        },
        error: function(x, e) {

        }
    });
       
   $('select[required]').css({
      display: 'inline',
      position: 'absolute',
      float: 'left',
      padding: 0,
      margin: 0,
      border: '1px solid rgba(255,255,255,0)',
      height: 0, 
      width: 0,
      top: '2em',
      left: '3em',
      opacity: 0,
      pointerEvents: 'none'
    });
$('select').formSelect();



   
 function setDetails()
    {
        //alert(document.getElementById('tarrif').value);
         document.getElementById('amount').value=document.getElementById('tarrif').value;
         document.getElementById('mobile_no_1').value=document.getElementById('mobile_no').value;
         document.getElementById('name_1').value=document.getElementById('name').value;
         
         document.getElementById('city_1').value=document.getElementById('city').value;
        document.getElementById('address_1').innerHTML=document.getElementById('street').value+" "+document.getElementById('address').value;
         document.getElementById('postcode_1').value=document.getElementById('searchbox').value;
         document.getElementById('c_mobile_no_1').value=document.getElementById('c_mobile_no').value;
         document.getElementById('c_name_1').value=document.getElementById('c_name').value;
         document.getElementById('d_postcode_1').value=document.getElementById('d_postcode').value;
         
         if(document.getElementById('d_city')!=null)
         {
            document.getElementById('d_city_1').value=document.getElementById('d_city').value;
         }
         else
         {
          document.getElementById('d_city_1').value=document.getElementById('d_city_P').value;
         }
         
         document.getElementById('d_address_1').innerHTML=document.getElementById('d_address').value;
         document.getElementById('postcode').value=document.getElementById('searchbox').value;
        
         
         //document.getElementById('tarrif').readOnly = true;
         
    }
    
function setTarrif()
    {
        var destination=document.getElementById('destination').value;
        var weight=document.getElementById('weight').value;
       
        if(weight=="")
        {
             Swal.fire(
                "Weight is required to caculate Tarrif"
            );
        }
        
    $.ajax(
    {
        type: "GET",
        url: 'get_zone'+'/'+destination+'/'+weight,
        dataType: "json",
        beforeSend: function(){
            document.getElementById('tarrif').style.backgroundImage='url(https://www.drivethrurpg.com/shared_images/ajax-loader.gif)';
        },
        complete:function(data){
            document.getElementById('tarrif').style.backgroundImage='none';
        },
        success: function(result) {
           
           document.getElementById('tarrif').value=result;
           if(document.getElementById('tarrif-error')!=null)
           {
            document.getElementById('tarrif-error').style.display="none";
            $('#tarrif').removeClass("invalid");
           }
          
          
        },
        error: function(x, e) {

        }
    });


    }
    function setVolume() {
      // `this` inside methods points to the Vue instance
     
     var length=document.getElementById('txtLength').value;
     var width=document.getElementById('txtWidth').value;
     var height=document.getElementById('txtHeight').value;

        if(length!="" && width!="" &&  height!="")
        { 
            document.getElementById('volume').value=length*width*height;
            document.getElementById('volume-error').style.display="none";
            $('#volume').removeClass("invalid");
        }
        else
        {
         
        }
    }
     

    function GetSelectedTextValue(country) {
        var selectedText = country.options[country.selectedIndex].innerHTML;
        var selectedValue = country.value;
        if(selectedText=="Pakistan")
        {
           $(".city_pakistan").show();
           $("input#d_city").hide();

        }
        else
        {
           $(".city_pakistan").hide();
           $("input#d_city").show();
        }
        //alert("Selected Text: " + selectedText + " Value: " + selectedValue);
    }

    function onlyNumberKey(evt) {
          
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
</script>

</script>

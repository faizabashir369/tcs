<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>TCS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="token" content="{{csrf_token()}}">
 @if (Auth::check()) 
         <meta name="user_id" content="{{ Auth::user()->email }}" />
 @endif 
         

<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">


    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
      <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

</head>

        <main class="py-4">
            @yield('content')
        </main>
    </div>



<script>
   function someFunction(destroyFeedback) {
      // Do your stuff here
      // Call destroyFeedback() function when you're done
      // The true parameter will proceed to the next step besides destroying the preloader
      setTimeout(() => {
         destroyFeedback(true);
      }, 1000);
   }

  


       
 $(document).ready(function(){
 $(function () {
                $('#dtpickerdemo').datetimepicker();
            });
var formData = new FormData($('#tracking-form')[0]);
    $('#tracking-form').bind('submit', function(e) {
           var spinner = $('#loader');
     
           var track=document.getElementById('tracking_id').value;
      e.preventDefault();
                       $.ajax({
                        type: 'get',
                        url: 'tracking?tracking_number='+track,
                        data: formData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        beforeSend: function(){
                            spinner.show();
                        },
                        complete:function(data){
                            spinner.hide();
                        }
                        })
                        .done  (function(response, textStatus, jqXHR)        
                        { 
                            console.log(response.meta.message);
                           if(response.meta.code=='201' || response.meta.code=='200')
                           {
                              Swal.fire(
    "Your Shipment Status is "+response.data.tracking.tag+" and expected to delivery on  "+response.data.tracking.expected_delivery
                                );
                           }
                           else
                           {
                                 Swal.fire(
                                 response.meta.message
                                );
                           }
                           
                            
                        })
                        .fail  (function(jqXHR, textStatus, errorThrown) 
                        {  
                            alert(errorThrown);
                            alert(textStatus);
                        });
   });

     $('.navbar-toggle').on('click',function(e)
{ 
   $('.navbar-collapse').hide();
});
    $('#mobile_no_1-error').hide();
$("SELECT").bind("change", function(e) {
    var id=this.id;
    id=id+"-error";
    if(id!=null)
    {
    document.getElementById(id).style.display="none";
}
    //$('#'.id).hide();
});
     $('#loader').hide();
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    $('form.require-validation').bind('submit', function(e) {
     
            var $form = $(".require-validation"),
            inputSelector = ['input[type=email]', 'input[type=password]',
                'input[type=text]', 'input[type=file]',
                'textarea'
            ].join(', '),
            $inputs = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('div.error'),
            valid = true;
            $errorMessage.addClass('hide');
            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();

                }
            });
            if (!$form.data('cc-on-file')) {
                e.preventDefault();
            
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
    });
    function stripeResponseHandler(status, response) {
        console.log(status);
        console.log(response);
        if (status!='200') {
          /*  $('.valid')
                .AddClass('invalid')
                .removeClass('valid')
                .text(response.error.message);
                */
                Swal.fire(
                 response.error.message
                );
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
            var $form = $(".require-validation");
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            var formData = new FormData($('#payment-form')[0]);
            var spinner = $('#loader');
                $.ajax({
                        type: 'post',
                        url: 'booking_form_submit',
                        data: formData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        beforeSend: function(){
                            spinner.show();
                        },
                        complete:function(data){
                            spinner.hide();
                        }
                        })
                        .done  (function(response, textStatus, jqXHR)        
                        { 
                            console.log(response);
                            result=JSON.parse(JSON.stringify(response));

                           
                            if(result.success=="1")
                            {
                                Swal.fire(
                                  result.message
                                ).then(function (result) {
                                      if (result.value) {
                                                
                                               
                                                location.href="/";
                                      } else {
                                        // handle cancel
                                      }
                                    })
                            }
                            else
                            {
                                Swal.fire(
                                  result.message
                                );
                            }
                        })
                        .fail  (function(jqXHR, textStatus, errorThrown) 
                        {  
                            alert(errorThrown);
                            alert(textStatus);
                            
                            
                        })
            //$form.get(0).submit();
        }
    }
  
   var user=document.querySelector("meta[name='user_id']");
   c_id=document.getElementById("customer_id");
   
   console.log(user);
   if(user!=null || user!=undefined || c_id!=null )
   {
    console.log("got");
    if(document.querySelector("meta[name='user_id']")!=null)
{    var id=document.querySelector("meta[name='user_id']").getAttribute('content'); 
        
        document.getElementById("customer_id").value=document.querySelector("meta[name='user_id']").getAttribute('content');
        id= "cpage?cid="+id;
        document.getElementById("cb").href=id;
        console.log(document.getElementById("cb").href);
        if(document.getElementById("booking_customer_id")!=null)
        {
            document.getElementById("booking_customer_id").value=document.querySelector("meta[name='user_id']").getAttribute('content');
        }


   }
}
   else
   {
         
   }
    
    });

    window.addEventListener("beforeunload", function (e) {
    $('#loader').show();
    
    });

 
</script>
<input name="customer_id" type="hidden"  id="customer_id">
</body>
</html>


<div class="container">
    <div class="row">
        <div class='col-lg-9'>
            <div class="form-group">
            <label for="dtpickerdemo" class="col-sm-2 control-label">Select date/time:</label>
                <div class='col-sm-4 input-group date' id='dtpickerdemo'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        
    </div>
</div>


</body>
</html>


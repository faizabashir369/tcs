@include('layouts.app')
@include('layouts.header')

<section id="content"> 
    <div class="container cpage">
    @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

           @if (session('message'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('message') }}
            </div>
           @endif
           @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('message') }}
            </div>
           @endif
           @if (session('error'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('error') }}
            </div>
           @endif
           @if (session('warning'))
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('warning') }}
            </div>
           @endif
            <h2>Customer Bookings</h2>
 <table class="table table-responsive">
        <thead>
          <tr>
              <th>Destination</th>
              <th>City</th>
              <th>Address</th>
              <th>No. of Pieces</th>
              <th>Weight</th>
              <th>Payment Mode</th>
              <th>Payment Status</th>
              <th>Payment</th>
          </tr>
        </thead>

        <tbody>
            <?php
            $email='';
 foreach($bookings as $b)
 {
    $email=$b->email;
     ?>

          <tr>
            <td> <?php echo $b->destination ?></td>
            <td> <?php echo $b->city ?></td>
            <td> <?php echo $b->d_address ?></td>
            <td> <?php echo $b->num_pieces ?></td>
            <td> <?php echo $b->weight ?></td>
            <td> <?php echo $b->pay_mode ?></td>
            <td> <?php echo $b->payment_status ?></td>
            <td> <?php echo $b->tarrif ?></td>
          </tr>
          <?php }
?>
        </tbody>
      </table>
      <br>
      <br>
      <br>
      <div class="d-flex justify-content-center">
            {!! $bookings->appends(['cid' => $email])->links() !!}
        </div>
   
</div>
</section>
@include('layouts.footer')
<script>
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
 function setDetails()
    {
        //alert(document.getElementById('tarrif').value);
         document.getElementById('amount').value=document.getElementById('tarrif').value;
         document.getElementById('mobile_no_1').value=document.getElementById('mobile_no').value;
         document.getElementById('name_1').value=document.getElementById('name').value;
         document.getElementById('postcode_1').value=document.getElementById('postcode').value;
         document.getElementById('city_1').value=document.getElementById('city').value;
         document.getElementById('address_1').value=document.getElementById('address').value;

         document.getElementById('c_mobile_no_1').value=document.getElementById('c_mobile_no').value;
         document.getElementById('c_name_1').value=document.getElementById('c_name').value;
         document.getElementById('d_postcode_1').value=document.getElementById('d_postcode').value;
         document.getElementById('d_city_1').value=document.getElementById('d_city').value;
         document.getElementById('d_address_1').value=document.getElementById('d_address').value;
         
        
         //document.getElementById('tarrif').readOnly = true;
         
    }
    
function setTarrif()
    {
        var destination=document.getElementById('destination').value;
        var weight=document.getElementById('weight').value;
       
        if(weight=="")
        {
             Swal.fire(
                "Weight is required to caculate Tarrif"
            );
        }
        
    $.ajax(
    {
        type: "GET",
        url: 'get_zone'+'/'+destination+'/'+weight,
        dataType: "json",
        beforeSend: function(){
            document.getElementById('tarrif').style.backgroundImage='url(https://www.drivethrurpg.com/shared_images/ajax-loader.gif)';
        },
        complete:function(data){
            document.getElementById('tarrif').style.backgroundImage='none';
        },
        success: function(result) {
           
           document.getElementById('tarrif').value=result;
          
          
        },
        error: function(x, e) {

        }
    });


    }
    function setVolume() {
      // `this` inside methods points to the Vue instance
     
     var length=document.getElementById('txtLength').value;
     var width=document.getElementById('txtWidth').value;
     var height=document.getElementById('txtHeight').value;
     if(length=="" || width=="" ||  height=="")
        {
             Swal.fire(
                "Length weight and height is required"
            );
        }
        else
        {
     document.getElementById('volume').value=length*width*height;
 }
    }
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
   <script type="text/javascript">
      $(function() {
    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]', 'input[type=password]',
                'input[type=text]', 'input[type=file]',
                'textarea'
            ].join(', '),
            $inputs = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('div.error'),
            valid = true;
        $errorMessage.addClass('hide');
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('hide');
                e.preventDefault();

            }
        });
        if (!$form.data('cc-on-file')) {
            e.preventDefault();
            
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
    });
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
           
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            var formData = new FormData($('#payment-form')[0]);
            var spinner = $('#loader');
                $.ajax({
                        type: 'post',
                        url: 'booking_form_submit',
                        data: formData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        enctype: 'multipart/form-data',
                        processData: false,
                        beforeSend: function(){
                            spinner.show();
                        },
                        complete:function(data){
                            spinner.hide();
                        }
                        })
                        .done  (function(response, textStatus, jqXHR)        
                        { 
                            
                            result=JSON.parse(JSON.stringify(response));

                           
                            if(result.success=="1")
                            {
                                Swal.fire(
                                  result.message
                                );
                            }
                            else
                            {
                                Swal.fire(
                                  result.message
                                );
                            }
                        })
                        .fail  (function(jqXHR, textStatus, errorThrown) 
                        {  
                            alert(errorThrown);
                            alert(textStatus);
                            
                            
                        })
            //$form.get(0).submit();
        }
    }
});
     
      
       
$('select[required]').css({
      display: 'inline',
      position: 'absolute',
      float: 'left',
      padding: 0,
      margin: 0,
      border: '1px solid rgba(255,255,255,0)',
      height: 0, 
      width: 0,
      top: '2em',
      left: '3em',
      opacity: 0,
      pointerEvents: 'none'
    });

   </script>
<header>
    <div class="account"><img src="img/account.png"><span>
                               
                               
                                     
                                     @if (Auth::check()) 
                <span class="dropdown">
<a href="#" data-toggle="dropdown" class="dropdown-toggle waves-effect waves-dark">My Account<b class="caret"></b></a>
                        <ul class="dropdown-menu profile">
                            <li><a class="waves-effect waves-dark" href="#">{{ Auth::user()->name }}</a></li>
                            <li><a class="waves-effect waves-dark" href="#">{{ Auth::user()->email }}</a></li>
                           
                                     <li><a class="waves-effect waves-dark" href="#">
                                <form action="{{ route('logout') }}" method="post">
<input type="submit"  id="logout" value="Logout" style="background-color: transparent !important;" >
                                </form>
                            </a></li>
                                
                        </ul>
                    </span> 
                     @else
              <a class="waves-effect waves-dark" href="{{ route('login') }}">My Account</a>
              @endif 
             

                                </span></div>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{ URL::asset('img/Logo.png')}}" class="logo"></a>
                   
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a class="waves-effect waves-dark" href="/">Home</a></li> 
                                                 <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle waves-effect waves-dark">About Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a class="waves-effect waves-dark" href="#">Mission & Core Values</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Milestones</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Leadership</a></li> 
                        </ul>
                    </li> 
                    <li><a data-toggle="dropdown" class="dropdown-toggle waves-effect waves-dark" href="#">Tracking <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a class="waves-effect waves-dark" href="cpage">Trace & Tracking Shipment</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Manage Home Delivery</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Manage Inbound / Outbound Shipment</a></li> 
                        </ul>


                       </li>
                    <li><a data-toggle="dropdown" class="dropdown-toggle waves-effect waves-dark" href="#">Shipping <b class="caret"></b></a>

                            <ul class="dropdown-menu">
                            <li><a class="waves-effect waves-dark" href="#">Create a Shipment</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Time & Cost</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Create a Return</a></li> 
                            <li><a  class="waves-effect waves-dark" href="">View Shipping History</a></li>
                            <li><a class="waves-effect waves-dark" href="#">Void Shipment</a></li>

                        </ul>
                        </li>
                        <li><a data-toggle="dropdown" class="dropdown-toggle waves-effect waves-dark" href="#">Services <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a class="waves-effect waves-dark" href="about.html">Domestic Delivery</a></li>
                                <li><a class="waves-effect waves-dark" href="#">Heavy Weight Delivery</a></li>
                                <li><a class="waves-effect waves-dark" href="#">Warehousing and Distribution</a></li> 
                                <li><a class="waves-effect waves-dark" href="#">Mail Management System</a></li>

                                 <li><a class="waves-effect waves-dark" href="#">Tracking Tools</a></li>
                                <li><a class="waves-effect waves-dark" href="#">TCS Cargo</a></li>


                                <li><a class="waves-effect waves-dark" href="#">International Delivery</a></li>
                                <li><a class="waves-effect waves-dark" href="#">Bulk Delivery</a></li>
                                <li><a class="waves-effect waves-dark" href="#">E-Com Solution</a></li>
                                <li><a class="waves-effect waves-dark" href="#">Self Service Centre</a></li>

                                <li><a class="waves-effect waves-dark" href="#">TCS Cool Control</a></li>
                                <li><a class="waves-effect waves-dark" href="#">TCS Travel and Visa</a></li>
                                

                            </ul>

                        </li>
                        <li><a id="cb" class="waves-effect waves-dark" href="login">Bookings</a></li>
                         
                    </ul>
                </div>
            </div>
        </div>
        </header>
         <header class="topbar">
    <div class="container">

      <div class="row">
        <!-- social icon-->
        <div class="col-sm-3">
           <ul class="social-network">
          <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-pinterest"></i></a></li>
          <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-google-plus"></i></a></li>
        </ul>
        </div>
        <div class="col-sm-9">
          <div class="row">
            <ul class="info"> 
                
              <li><span><a href="tel:+9221111123456" class="text-light">Pakistan : + 92 21 111 123 456</a> </span> <span><a href="tel:+971600565651" class="text-light">UAE : + 971 600565651</a>  </span><span><a href="tel:+442088495600" class="text-light">UK : +(44) 208 8495600  </a></span></li>

            </ul>
            <div class="clr"></div>
          </div>
        </div>
        <!-- info -->

      </div>
    </div>
  </header>
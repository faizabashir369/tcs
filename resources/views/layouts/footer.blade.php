<div id="loader"></div>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="widget">
          <h5 class="widgetheading">Our Contact</h5>
          TCS Express WorldWide (UK) Limited
          1000 Great West Road, Brentford,
          Middlesex TW8 9HH,
          London, UK 
          CS Hotline: +(44) 208 8495600
         
        </div>
      </div>
      <div class="col-sm-3">
        <div class="widget">
          <h5 class="widgetheading"><a href="#">About Us</a></h5>
          <ul class="link-list">
            <li><a class="waves-effect waves-dark" href="#">Mission &amp; Core Values</a></li>
            <li><a class="waves-effect waves-dark" href="#">Milestones</a></li>
            <li><a class="waves-effect waves-dark" href="#">Leadership</a></li>
            <li><a class="waves-effect waves-dark" href="#">Career</a></li>
            <li><a class="waves-effect waves-dark" href="#">Careers</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="widget">
          <h5 class="widgetheading">Services</h5>


          <ul class="link-list">
            <li><a class="waves-effect waves-dark" href="#">Domestic Delivery</a></li>
            <li><a class="waves-effect waves-dark" href="#">Warehousing and Distribution</a></li>
            <li><a class="waves-effect waves-dark" href="#">Mail Management System</a></li>
            <li><a class="waves-effect waves-dark" href="#">TCS Cargo</a></li>
            <li><a class="waves-effect waves-dark" href="#">TCS Cool Control</a></li>
            <li><a class="waves-effect waves-dark" href="#">TCS Travel and Visa</a></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="widget">
          <h5 class="widgetheading">Portfolio Companies</h5>
          
          <ul class="link-list">
            <li><a class="waves-effect waves-dark" href="https://sentimentsexpress.com/">SENTIMENTS</a></li>
            <li><a class="waves-effect waves-dark" href="http://www.octara.com/">OCTARA</a></li>
            <li><a class="waves-effect waves-dark" href="https://pk.studiobytcs.com/">STUDIO BY TCS</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="sub-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="copyright">
          <p>
              <span>&copy; 2021 TCS (Private) Limited - All rights reserved </span>
            </p>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="social-network">
            <li><a class="waves-effect waves-dark" href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a class="waves-effect waves-dark" href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a class="waves-effect waves-dark" href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="waves-effect waves-dark" href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li><a class="waves-effect waves-dark" href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  

  </footer>
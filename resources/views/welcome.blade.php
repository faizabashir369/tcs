@include('layouts.app')
@include('layouts.header')

<div class="container">
  
  <div id="wrapper" class="home-page"> 
  
  <!-- start header -->
 
  <!-- end header -->
           @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

           @if (session('message'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('message') }}
            </div>
           @endif
           @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('message') }}
            </div>
           @endif
           @if (session('error'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('error') }}
            </div>
           @endif
           @if (session('warning'))
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
            {{ session('warning') }}
            </div>
           @endif
  <section id="banner">
   
  <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img src="img/slides/1.png" alt="" />
                
                <div class="flex-caption">

                    <div class="inter">
                        <h2>Send Internationally</h2>
                        </div>
                    <form action="quote_booking" method="post" id="quoteForm" name="quoteForm" class="quoteForm">
                        @csrf
                        
                        <div class="row">
                            <div class="col s12 m4 l4">
                               
                        <select name="weight" id="weight" class="form-control1 required validate valid" required>
<option value="" selected disabled><i class="fa fa-facebook"></i>PARCEL  WEIGHT</option>
                                            <option value="0.5">0.5</option>
                                            <option value="1.0">1.0</option>
                                            <option value="1.5">1.5</option>
                                            <option value="2.0">2.0</option>
                                            <option value="2.5">2.5</option>
                                            <option value="3.0">3.0</option>
                                            <option value="3.5">3.5</option>
                                            <option value="4.0">4.0</option>
                                            <option value="4.5">4.5</option>
                                            <option value="5.0">5.0</option>
                                  </select>
                            </div>
                            <div class="col s12 m5 l4"  id="sendto">
                            
                                                <select name="destination" id="destination" class="form-control1 required validate valid" required>

                                                 
                                                <option value="" selected disabled>SEND TO</option>
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua">Antigua</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Azores">Azores</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Barbuda">Barbuda</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bonaire">Bolivia</option>
                                                <option value="Bonaire">Bonaire</option>
                                                <option value="Bosnia Herzegovina">Bosnia Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="Brunei">Brunei</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Canary Islands">Canary Islands</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Cent. African Rep">Central African Rep</option>Cent. African Rep
                                                <option value="Channel Islands">Channel Islands</option>Comoros
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros Islands">Comoros Islands</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Congo Dem. Republic">Congo Dem. Republic</option>
                                                <option value="Cook Islands">Cook Islands</option>

                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Curacao">Curacao</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Rep">Dominican Rep</option>
                                                <option value="East Timor">East Timor</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands">Falkland Islands</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France </option>
                                                <option value="French Guyana">French Guyana</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam </option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland (Republic of)">Ireland (Republic of)</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Ivory Coast">Ivory Coast</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea (South)">Korea (South)</option>
                                                <option value="Korea (North)">Korea (North)</option>
                                                <option value="Kosovo">Kosovo</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macau">Macau</option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Montenegro">Montenegro</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Monserrat">Monserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>Palestine State
                                                <option value="Niue">Niue</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palestine State">Palestine State</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines">Philippines</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion Island">Reunion Island</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russian Federation">Russian Federation</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Saipan">Saipan</option>
                                                <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore </option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="St Lucia">St Lucia</option>
                                                <option value="St Maarten">St Maarten</option>
                                                <option value="St Vincent">St Vincent</option>
                                                <option value="St Barthelemy">St. Barthelemy</option>
                                                <option value="St Eustatius">St. Eustatius</option>
                                                <option value="St Kitts & Nevis">St. Kitts &amp; Nevis</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>

                                                <option value="Syria">Syria</option>
                                                <option value="Tahiti">Tahiti</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks & Caicos Is.">Turks & Caicos Is.</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="U.A.E">UAE</option>
                                                
                                                <option value="Uganda">Uganda</option>
                                                <option value="UK">UK</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="U.S. of America">U.S. of America</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands (Brit)">Virgin Islands</option>
                                                <option value="Virgin Islands (US)">Virgin Islands (US)</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                                        </select>
                                                          
                                            </div>
                                        <div class="col s6 m3 14 get-quote">
                                <button type="submit" class="waves-effect waves-dark btn next-step quote_btn">GET QUOTE</button>
                                        </div>
                                        </div>

                            
                          
                    </form>
                    <form id="tracking-form" method="get" class="mobile_hidden" action="tracking">

                         <div id="track">
                        <input type="text" name="tracking_number" id="tracking_id" placeholder="Type your tracking number">
                        <button class="track_btn">Track <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M19.5268 10.3604V10.4759L19.4113 10.5915L19.2957 10.8996H19.6424L19.989 10.784L20.2201 10.553H19.989L19.8735 10.3219L19.6424 10.1293V9.70564L19.3343 9.82118L19.2187 9.93673V10.0138L19.3343 10.1293L19.5268 10.3604ZM27.8074 8.35763C27.5763 8.58872 26.1513 8.12655 24.4952 7.24072C24.3796 7.12517 24.2641 7.12517 24.1486 7.00963L18.641 9.08941L18.2173 8.97387L22.2999 6.00825C21.1829 5.23796 19.8735 4.46768 19.1032 3.88996L17.216 5.00688L17.1004 4.89133L18.641 3.4663C18.5254 3.35076 18.5254 3.35076 18.5254 3.27373C18.5254 3.27373 18.641 3.27373 18.7565 3.27373L19.0646 1.27098H19.1802L19.4113 3.4663C20.1816 3.81293 21.6066 4.35213 22.9161 5.00688L23.0702 0L23.4938 0.231087L24.6878 5.96974C24.8033 6.08528 25.0344 6.20083 25.1114 6.20083C26.6905 7.04814 28 8.04952 27.8074 8.35763ZM1.65612 16.7538C1.65612 9.51307 7.51032 3.54333 14.9051 3.54333C15.5598 3.54333 16.9078 3.77442 17.6781 3.96699L17.9092 2.96561L18.795 2.42641C17.7552 2.31087 15.7909 1.88721 14.9051 1.88721C6.62448 1.88721 0 8.58872 0 16.7538C0 20.8363 3.1967 28 7.81843 28C4.42916 25.458 1.65612 21.4911 1.65612 16.7538ZM5.08391 12.132V12.4787V13.0949L5.50757 13.326V14.0963L5.93122 14.751L6.16231 14.8666L6.27785 14.5199L5.93122 13.9807L5.81568 13.4415H6.04677L6.16231 13.9807L6.70151 14.8666L6.58597 15.0591L6.9326 15.6369L7.81843 15.8294V15.7139H8.16506V15.945L8.39615 16.0605L8.81981 16.1761L9.47455 16.8308L10.2448 16.9464L10.3604 17.6011L9.82118 17.9092V18.4869L9.70564 18.795L10.4759 19.7964V20.1431H10.8226L11.4773 20.6823V22.3384L11.7084 22.4539L11.4773 23.2242L11.901 23.7634L11.7854 24.5337L12.2091 25.304L12.8638 25.8432L13.5186 25.9587V25.7276L13.0949 25.2655V25.1499L13.2105 24.8033V24.5722H12.9023L12.7868 24.3411L13.0179 24.11V23.9945L12.7868 23.879V23.7634L13.2105 23.6479L13.7497 23.3398L13.9807 22.9931L14.6355 22.2228L14.4044 21.4525L14.6355 21.26H15.1747L15.5983 20.9133L15.7139 19.7194L16.1376 19.1802L16.2531 18.718L15.7909 18.6025L15.5598 18.1788H14.674L13.9037 17.9862V17.4085L13.6726 17.1004H13.0179L12.7098 16.4457L12.3631 16.3301L12.2476 16.4457H11.7084L11.4773 16.2146L10.9381 16.1761L10.5144 16.7153L9.62861 16.5997V15.7139L8.97387 15.5983L9.20495 15.0206V14.8281L8.43466 15.3673L7.89546 15.2517L7.77992 14.9051L7.89546 14.4814L8.12655 14.0578L8.78129 13.7111H9.97524L9.8597 14.0578L10.3989 14.2889L10.2834 13.6341L10.63 13.403L11.2847 12.9409V12.6327L11.9395 11.978L12.5942 11.6314V11.5543L12.9409 11.2077H13.1334L13.249 11.3232L13.3645 11.1307H13.4801H13.249L13.0179 11.0151V10.784H13.1334H13.3645H13.4801L13.5956 11.0151H13.7111V10.8996V11.0151L14.0578 10.8996L14.1733 10.784H14.2503V11.0151L14.1348 11.1307V11.3232L14.674 11.4388H14.7895V11.2077L14.3659 10.9766V10.8611L14.7125 10.6685V10.3219L14.3659 10.0908L14.2503 9.5901L13.7882 9.82118H13.5571V9.39752L12.9023 9.16644L12.5557 9.39752V10.1678L12.0165 10.2834L11.7854 10.707L11.5928 10.784V10.1293H11.1307L10.8226 9.93673L10.707 9.5901L11.7084 9.05089L12.0165 8.93535L12.132 9.16644H12.3246V9.05089H12.6713V8.93535H12.5557V8.74278H12.7868L13.0179 8.51169H13.1334L13.7882 8.39615L14.1348 8.62724L13.3645 9.08941L14.2503 9.24347L14.4814 8.89684H14.9051L15.0206 8.58872H14.7125V8.28061L13.7111 7.85695H13.0564L12.7098 8.04952L12.8253 8.51169H12.3631L12.2476 8.28061L12.6713 7.85695H12.0165L11.7854 7.97249L11.6699 8.20358H11.8624V8.43466H11.4388L11.3232 8.66575H10.784L10.6685 8.35763H11.2077L11.5543 8.011L11.3232 7.89546L11.0922 8.08803H10.6685L10.3219 7.66437H9.78267L9.12792 8.20358H9.78267V8.31912L9.66713 8.43466H10.2063L10.3219 8.66575H9.66713V8.47318L9.12792 8.35763L9.01238 8.24209H8.47318C10.1293 6.9326 12.2091 6.23934 14.4429 6.23934C16.3301 6.23934 18.0633 6.77854 19.6039 7.54883L20.3741 7.00963C18.6025 6.00825 16.6382 5.35351 14.4429 5.35351C11.5928 5.35351 8.93535 6.47043 6.9326 8.31912C6.47043 8.66575 6.04677 9.08941 5.62311 9.62861C4.19807 11.5158 3.31224 13.8267 3.31224 16.4842C3.31224 22.5309 8.28061 27.4993 14.4429 27.4993C18.7565 27.4993 22.4924 25.0729 24.3796 21.5681C24.7263 20.7978 25.0344 19.912 25.2655 19.0261C25.381 18.795 25.381 18.6025 25.381 18.3714C25.4966 17.8322 25.5736 17.1774 25.5736 16.4842C25.5736 16.3686 25.5736 16.3686 25.5736 16.3686C25.5736 16.0605 25.5736 15.7139 25.5736 15.3673C25.458 14.2503 25.2655 13.2875 24.8033 12.2861C24.8033 12.2861 24.6878 12.1706 24.6878 12.055C24.2641 10.9381 23.4938 9.74415 22.608 8.85832L22.4924 8.74278C22.2613 8.51169 22.1458 8.31912 21.9147 8.20358C21.9147 8.20358 21.9147 8.20358 21.7992 8.20358L20.7978 8.51169C20.9133 8.62724 21.0289 8.62724 21.1444 8.74278L21.0289 8.97387L20.6437 9.24347L20.4127 9.47455L20.5282 9.66713H20.7593L20.8748 10.0908L21.337 9.89821L21.4525 10.4374H21.1829L20.8363 10.3219L20.5282 10.4374L20.1045 10.8611L19.6424 11.0151L19.5268 11.4388L19.7579 11.5543L19.6424 11.7854L19.2187 11.6699L18.6795 11.7854V12.132V12.6713L19.0261 12.7868H19.4498H19.7579L19.8735 12.5557L20.2971 11.901H20.6437L20.9519 11.6699L21.0674 11.901L21.8377 12.3246L21.7221 12.5172H21.414L21.5296 12.6327L21.7607 12.7483L21.9917 12.6327V12.3246L22.2228 12.2091L22.1073 12.0935L21.5681 11.901L21.4525 11.4388H21.7607L21.9532 11.5543L22.2999 11.978V12.3246L22.7235 12.7483L22.8391 12.2091L23.1472 12.0935V12.5172L23.4938 12.7483H23.9175C24.033 13.0564 24.1486 13.2875 24.2641 13.5186V13.6341H23.3783L22.9161 13.2875H22.3769V13.6341H22.2613L22.0303 13.5186L21.1829 13.326V12.7868H19.989L19.6424 12.9794H19.2187H19.0261L18.4484 13.326V13.8652L17.2545 14.6355L17.37 14.9821H17.7166L17.6011 15.3287L17.3315 15.4058V16.2916L18.3329 17.4085H18.795V17.293H19.5653L19.7964 17.0619H20.2586L20.4512 17.293L21.1059 17.4085L20.9904 18.2944L21.7607 19.4883L21.4525 20.2586V20.6052L21.6836 20.8363V21.7221L22.1073 22.2613V22.9161H22.4539C20.5667 25.227 17.7166 26.652 14.5199 26.652C8.89684 26.652 4.35213 22.1458 4.35213 16.5227C4.35213 15.0977 4.69876 13.6726 5.12242 12.4402V12.132L5.46905 11.7084C5.58459 11.4773 5.81568 11.2847 5.89271 11.0536V11.2847L5.46905 11.7084L5.08391 12.132ZM23.4938 11.901H23.6094C23.7249 12.0165 23.7249 12.0935 23.8404 12.3246H23.7249H23.4938V11.901ZM21.8377 9.93673V9.51307C22.0688 9.70564 22.2613 9.82118 22.3769 10.0523L22.1458 10.3989H21.4911V10.2834L21.8377 9.93673ZM8.51169 8.70426H8.74278H9.08941V8.81981V8.93535H8.55021V8.70426H8.51169ZM8.81981 9.24347L9.05089 9.12792V9.47455H8.62724L8.51169 9.35901L8.81981 9.24347ZM9.39752 10.4759L9.70564 10.2834L9.93673 10.3989L9.82118 10.7455H9.5901L9.39752 10.4759ZM10.8226 11.2462V11.3618H10.2834H10.0523V11.2462L10.3989 11.0151H10.7455V11.2462H10.8226ZM11.1307 11.4388V11.6699L10.8996 11.7854H10.784V11.4773H11.1307V11.4388ZM10.9381 11.3618V11.1692L11.1692 11.3618H10.9381ZM11.0536 11.901V12.0935L10.9381 12.2091H10.63V11.978H10.8611V11.8624H11.0536V11.901ZM10.2448 11.4388H10.5915L10.1293 12.0935L9.89821 11.978L10.0138 11.6314L10.2448 11.4388ZM11.5928 11.7854V12.0165H11.2462V11.901V11.6699L11.5928 11.7854ZM11.2462 11.5543L11.3618 11.4388L11.5928 11.5543L11.4773 11.6699L11.2462 11.5543ZM24.3796 14.2118V14.0963C24.4952 14.2118 24.4952 14.2118 24.4952 14.3274L24.3796 14.2118ZM12.9023 11.6699L12.7868 11.7854H13.0179L13.1334 11.6699L12.9023 11.5543V11.6699ZM13.5571 11.3618V11.5543H13.249V11.6699H13.3645C13.3645 11.6699 13.3645 11.6699 13.3645 11.7854H13.5571L13.6726 11.6699V11.5543H13.7882L13.9037 11.3232H13.7882H13.5571V11.3618ZM19.2957 10.4759V10.2834L19.0646 10.1678L18.8721 10.2448L18.641 10.553V10.6685H18.8721L19.2957 10.4759Z" fill="white"/>
</svg>
</button>
                             </div>   
                    </form>

                    
                    
                </div>
              </li>
              
            </ul>
        </div>
  <!-- end slider --> 
  </section>  
  <section class="desktop_hidden">
     
                    <form action="quote_booking" method="post" id="quoteForm" name="quoteForm" class="quoteForm">
                        @csrf
                        <h2>Send Internationally</h2>
                       
                        <div class="row">
                            <div class="col s12 m12 l4">
                                <div class="row">
                            
                                    <div class="col s12 m12 l4">
                                          <select name="weight" id="weight" class="form-control1 required validate valid" required>
                                                    <option value="" selected disabled>PARCEL WEIGHT</option>
                                                    <option value="0.5">0.5</option>
                                                    <option value="1.0">1.0</option>
                                                    <option value="1.5">1.5</option>
                                                    <option value="2.0">2.0</option>
                                                    <option value="2.5">2.5</option>
                                                    <option value="3.0">3.0</option>
                                                    <option value="3.5">3.5</option>
                                                    <option value="4.0">4.0</option>
                                                    <option value="4.5">4.5</option>
                                                    <option value="5.0">5.0</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m12 l4">
                                
                                <div class="row">
                                    <div class="col s12 m12 l4">
                                                <select name="destination" id="destination" class="form-control1 required validate valid" required>

                                                 
                                                <option value="" selected disabled>SEND TO</option>
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua">Antigua</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Azores">Azores</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Barbuda">Barbuda</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bonaire">Bolivia</option>
                                                <option value="Bonaire">Bonaire</option>
                                                <option value="Bosnia Herzegovina">Bosnia Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="Brunei">Brunei</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Canary Islands">Canary Islands</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Cent. African Rep">Central African Rep</option>Cent. African Rep
                                                <option value="Channel Islands">Channel Islands</option>Comoros
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros Islands">Comoros Islands</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Congo Dem. Republic">Congo Dem. Republic</option>
                                                <option value="Cook Islands">Cook Islands</option>

                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Curacao">Curacao</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Rep">Dominican Rep</option>
                                                <option value="East Timor">East Timor</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands">Falkland Islands</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France </option>
                                                <option value="French Guyana">French Guyana</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam </option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland (Republic of)">Ireland (Republic of)</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Ivory Coast">Ivory Coast</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea (South)">Korea (South)</option>
                                                <option value="Korea (North)">Korea (North)</option>
                                                <option value="Kosovo">Kosovo</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macau">Macau</option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Montenegro">Montenegro</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Monserrat">Monserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>Palestine State
                                                <option value="Niue">Niue</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palestine State">Palestine State</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines">Philippines</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion Island">Reunion Island</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russian Federation">Russian Federation</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Saipan">Saipan</option>
                                                <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore </option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="St Lucia">St Lucia</option>
                                                <option value="St Maarten">St Maarten</option>
                                                <option value="St Vincent">St Vincent</option>
                                                <option value="St Barthelemy">St. Barthelemy</option>
                                                <option value="St Eustatius">St. Eustatius</option>
                                                <option value="St Kitts & Nevis">St. Kitts &amp; Nevis</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>

                                                <option value="Syria">Syria</option>
                                                <option value="Tahiti">Tahiti</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks & Caicos Is.">Turks & Caicos Is.</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="U.A.E">UAE</option>
                                                <option value="U.A.E">UAE</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="U.S. of America">U.S. of America</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands (Brit)">Virgin Islands</option>
                                                <option value="Virgin Islands (US)">Virgin Islands (US)</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                                        </select>
                                                          
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m12 14 center">
                                            <button type="submit" class="waves-effect waves-dark btn next-step quote_btn">Submit</button>
                                            </div>
                                        </div>    
                    </form>
                </div>
                <form id="tracking-form">

                        <div class="tr">
                                    <input type="text" id="tracking_id" placeholder="Enter tracking number">
                                    <button class="track_btn">Track</button>
                        </div>
                                
                    </form>



                     
  </section>

  
    <section class="dishes">
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="aligncenter"><h2 class="aligncenter">TCS UK</h2>
        <p>TCS has a strong presence not only in Pakistan but also in the UK. TCS UK provides a range of secure and reliable services with the aim of serving the Pakistani diaspora in UK and connecting Pakistanis and individuals around the world.</p>

 
<p>
TCS offers international delivery services - International Documents and Parcel Express Delivery - to over 3,500 destinations across the world through its extended International Network.
</p>
</div>
        <br/>
      </div>
    </div>
 
  
    
    </section>





  <section id="content"> 
  <div class="container">
  
    <div class="row features">
        <div class="col s12 m6 l6 center">
            <img src="img/studio2.png">
            <p>TCS Sentiments continues to strengthen relationships and spread happiness by allowing people to send gifts, cakes and flowers to Pakistan.</p>
            <button class="btn_feature">Find out more</button>
        </div>
        <div class="col s12 m6 l6 center">
            <img src="img/studio.png">
            <p>Studio by TCS showcases carefully curated collections from an array of Pakistani designers and brings the best of Pakistani fashion to your doorstep.</p>
            <button class="btn_feature">Find out more</button>
        </div>
    </div>
    <div class="row features">
        <div class="col s12 m6 l6 center">
            <img src="img/Vector.png">
            <p>You can book your shipments online for delivery to Pakistan or any other international destination..</p>
            <button class="btn_feature">Find out more</button>
        </div>
        <div class="col s12 m6 l6 center">
            <img src="img/phone.png" class="phn">
            <p>In case of any queries, you can call our CS hotline or fill in the feedback form..</p>
            <button class="btn_feature">Find out more</button>
        </div>
    </div>

<a href="#" class="scrollup waves-effect waves-dark"><i class="fa fa-angle-up active"></i></a>
</div>
</section>
<!-- navbar -->
    <script src="{{ mix('/all.js') }}">
    </script>
<script>
    $('select').material_select();
    
     
</script>

const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/js/app.js',
         'public/js/jquery.js',
         'public/js/jquery.easing.1.3.js',
         'public/materialize/js/materialize.min',
         'public/js/bootstrap.min.js',
         'public/js/jquery.fancybox.pack.js',
         'public/js/jquery.fancybox-media.js',
         'public/js/jquery.flexslider.js',
         'public/js/animate.js',
         'public/js/modernizr.custom.js',
         'public/js/jquery.isotope.min.js',
         'public/js/jquery.magnific-popup.min.js',
          'public/js/mstepper.min.js',
         'public/js/custom.js'
         ], 'public/all.js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');
mix.styles(['public/materialize/css/materialize.min.css',
            'public/css/bootstrap.min.css',
            'public/css/fancybox/jquery.fancybox.css',
            'public/css/mstepper.min.css',
            'public/css/flexslider.css,css/style.css'],
            'public/all.css');

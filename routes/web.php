<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\VerifyController;
use App\Http\Controllers\TrackingController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('date', function () {
    return view('datetimepicker');
});
Route::get('page', function () {
    //return view('booking_page');
});

Route::get('uploadFile',[Controller::class, 'uploadFile']);
Route::get('confirm-booking/{id}/{cid}',[BookingController::class, 'confirmBooking']);
Route::get('cpage', [BookingController::class, 'getBookings'])->name('getBooking')->middleware('auth');
Route::get('booking', function () {
    return view('booking');
});

Route::get('txtData', [Controller::class, 'downloadTxtData']);
Route::get('verifyAccount/{token}', [Controller::class, 'VerifyEmail']);

Route::any('quote_booking',[BookingController::class, 'quote_form'])->name('quote_form.post');

Route::get('/signup', function () {
    return view('registration');
});
Route::get('home', function () {
    return view('welcome');
});
Route::get('adminViewBookings', [BookingController::class, 'getAdminBookings'])->name('getAdminBooking')->middleware('auth');

Route::get('admin', function () {
    return view('admin_template');
});

Route::get('stripe', [StripeController::class, 'stripe']);
Route::get('tracking', [BookingController::class, 'tracking']);


Route::get('/verify/{token}','Controller@VerifyEmail');

Route::get('stripe', [StripeController::class, 'stripe']);
Route::post('stripe_result', [StripeController::class, 'stripePost'])->name('stripe.post');
Route::post('booking_form_submit', [BookingController::class, 'save_data'])->name('Booking.post');
Route::get('get_zone/{destination}/{weight}', [BookingController::class, 'get_price'])->name('BookingController');

Auth::routes(['verify' => true]);
Auth::routes();


//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct($user)
    {
        $this->user = $user;
        return $this->view('verifyEmail')->with(['name'=>$user->name,'email_verification_token'=>$user->email_verification_token]);  
    }

    public function build()
    {
        

    }
}
?>
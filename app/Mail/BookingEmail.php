<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct($request,$id,$create)
    {
        $this->request = $request;
        return $this->view('BookingEmail')->with(['request'=>$request,'id'=>$id,'create'=>$create]);  
    }

    public function build()
    {
        

    }
}
?>
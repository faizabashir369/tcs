<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'customer_id',
        'comp_name',
        'postcode',
        'email',
        'origin',
        'city',
        'address',
        'street',
        'c_mobile_no',
        'c_name',
        'c_comp_name',
        'destination',
        'd_postcode',
        'd_city',
        'd_address',
        'num_pieces',
        'country_code',
        'ship_content',
        'ship_vaue',
        'weight',
        'volume',
        'service_code',
        'agent_code',
        'instructions',
        'pay_mode',
        'tarrif',
        'mobile_no'


        // add all other fields
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'booking_form';
}

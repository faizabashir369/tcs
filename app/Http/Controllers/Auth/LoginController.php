<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        session(['url.intended' => url()->previous()]);
        $this->redirectTo = session()->get('url.intended');

        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
       /* 
        if (!Auth::attempt(['email_verified' => '1'])) {
            // Authentication passed...
            Auth::logout();
            session()->flash('warning', 'Your Account is Inactive Please Activate your account to login');
           return;
        }
        */
        $user = User::where(['email' => $request->email, 'email_verified' => '1'])->first();
    if(!$user){
        session()->flash('warning', 'Your Account is Inactive Please Activate your account to login');
           return redirect()->to('login');
    }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            session()->flash('message', 'Login Successful');
             return redirect()->back()->withInput()->withSuccess([ 'email' => "Login Successful." ]);
            }
             else
            {
                session()->flash('error', 'These credentials do not match our records.');
                return redirect()->back()->withInput()->withErrors([ 'email' => "These credentials do not match our records." ]);
            }
     
    }

   public function logout()
   {
      Auth::logout();
      return redirect('/');
   }

}

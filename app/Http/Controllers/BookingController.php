<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Mail\BookingEmail;
use App\Models\Booking;
use Stripe;
use Session;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Mail;

class BookingController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index()
    {

    }
    public function getBookings(Request $request)
    { 
        $cid =$request->input('cid');
        
        $bookings=DB::table('booking_form')->orderBy('id', 'desc')->where('email',$cid)->paginate(15);
        return view('booking_page')->with('bookings',$bookings); 
    }
    public function save_data(Request $request)
        {    
           
  Stripe\Stripe::setApiKey('sk_test_51B8owIBhzweLs6c9IkKSZZYAn5jU2einifmepws03LTZMxXkkRHS3rzCQ7DfbnS2Jr4hyMpGJHn0JirLrdq5O4v700uM35JEQo');
    $result=    Stripe\Charge::create ([
                "amount" => $request->tarrif * 100,
                "currency" => "GBP",
                "source" => $request->stripeToken,
                "description" => "TCS Booking Payment"
        ]);

        $booking = new Booking; 
     
        try
        {
            //$user=DB::table('booking_form')->insert($request->all());
            $user = booking::create($request->all());
            
            $id = DB::getPdo()->lastInsertId();
            $status="1";
            $message="Successfuly Submitted";
        }
        catch(Exception $e)
        {
            $status="0";
            $message="Failed to Book";

        }

  // echo "resultis ".$result->status;
 //  echo "message is ".$message.$status;
 //  echo "user is".$user;
 // ptint_r($result);
        
        if($result->status=="succeeded")
          {

            $update= booking::where('id', $id)->update(['payment_status' => 'Payed']);

            $data = new \stdClass();
      $tracking_number = new \stdClass();
      $tracking_number->tracking_number="0".$id;
      $tracking_number->slug= "dpd";
      $data->tracking=$tracking_number;

       $curl = curl_init();

      curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.aftership.com/v4/trackings",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER => [
          "aftership-api-key: 3ebb3212-4f9a-4927-9d04-0dec983f4a58",
          "Accept: application/json",
          "Content-Type: application/json"
        ],
      ]);

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
      
        $status="1";

      } else {
        $status="0";
        $message="could not create tracking";
      }




            $status="1";
            $message="Payment Successful";

            $data = array('data'=>'dtaaa');

             $user = DB::table('users')->where('email',$request->email)->first();

                  if(!$user){
                         //user is not found 
                   // $res=\Mail::to($request->email)->send(new createAccountEmail($request));

                    $create=1;
                  }
                  if($user){
                         // user found 
                    //$res=\Mail::to($request->email)->send(new createAccountEmail($request));
                    $create=0;
                  }


            try{


             $res=\Mail::to($request->email)->send(new BookingEmail($request,$id,$create));

            }
            catch(Exception $e)
            {

            }
             

             if(\Mail::failures()) {

               $message=$message."There was one or more failures. They were: <br />";

               foreach(Mail::failures() as $email_address) {
                   $message=$message." - $email_address <br />";
                }

            } else {
               // $message=$message."No errors, all sent successfully!";
            }
            
            
          }
          else
          {
            
            $status="0";
            $message="Payment Unsuccessful";
            
          }
          return json_encode(array("success" => $status,
                                    "message" => $message,
                                    "result" => $result),JSON_UNESCAPED_UNICODE);
    }
    public function quote_form(Request $request)
    {
   
    $weight=$request->weight;
    $destination=$request->destination;
         return view('booking')->with(['weight'=>$request->weight,'destination'=>$destination]);  

    } 
    public function get_price($country, $weight)
        {  
         try
              {
                  $result = DB::table('country_zones')->where('country' , $country)->first();
                  if($result)
                  {
                    $result1 = DB::table('tbltariffcalc')->where('Zone' , $result->zone);
                     $result1=$result1->where(  'Weight' , $weight);
                     $result1=$result1->first();
                    return json_encode($result1->Rate);
                  }
                  else
                  {

                    return json_encode(array('result'=> 'Not found'));
                  }
              }
              catch(Excwption $e)
              {
                    return json_encode(array('result'=> 'An error ocured'));
              }
              
        }

   
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
       /* Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100 * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "This payment is tested purpose"
        ]);
   
        //Session::flash('success', 'Payment successful!');
           
        echo 'Payment successful';
        */
    }
    public function confirmBooking($id,$cid)
    {
       $update= booking::where('id', $id)->update(['confirmed' => '1']);
       $cid =$cid;
        
        $bookings=DB::table('booking_form')->where('email',$cid)->get();

       if($update)
       {
          session()->flash('message', 'Your Booking is Confirmed');

          return view('booking_page')->with('bookings',$bookings); 
         // return redirect('/');
        
       }
       else
       {
          session()->flash('error', 'Failed to confirm Booking.');
          return redirect('/');
       }
    }
    public function tracking(Request $request)
    {
      $data = new \stdClass();
      $tracking_number = new \stdClass();
      $tracking_number->tracking_number=$request->tracking_number;
      $tracking_number->slug= "dpd";
      $data->tracking=$tracking_number;
 $curl = curl_init();
curl_setopt_array($curl, [
  CURLOPT_URL => "https://api.aftership.com/v4/trackings/dpd/".$request->tracking_number,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => [
    "aftership-api-key: 3ebb3212-4f9a-4927-9d04-0dec983f4a58",
    "Accept: application/json",
    "Content-Type: application/json"
  ],
]);

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
/*

      $curl = curl_init();

      curl_setopt_array($curl, [
        CURLOPT_URL => "https://api.aftership.com/v4/trackings",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER => [
          "aftership-api-key: 3ebb3212-4f9a-4927-9d04-0dec983f4a58",
          "Accept: application/json",
          "Content-Type: application/json"
        ],
      ]);

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        echo $response;
      }
      */
    }
    
}

<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class RestoController extends Controller
{
    //
    function registerUser(Request $req){
        $validateData = $req->validate([
        'name' => 'required|regex:/^[a-z A-Z]+$/u',
        'email' => 'required|email',
        'password' => 'required|min:6|max:12',
        'confirm_password' => 'required|same:password',
        'mobile' => 'numeric|required|digits:10'
        ]);
        $result = DB::table('users')->where('email',$req->input('email'))->get();

        $res = json_decode($result,true);
        print_r($res);

            if(sizeof($res)==0){
            $data = $req->input();
            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $encrypted_password = crypt::encrypt($data['password']);
            $user->password = $encrypted_password;
            //$user->mobile = $data['mobile'];
            $user->save();
            $req->session()->flash('register_status','User has been registered successfully');
            return redirect('/register');
            }
            else{
            $req->session()->flash('register_status','This Email already exists.');
            return redirect('/register');
            }
}

}
